# class Car:
#     def __init__(self, make, model, year):
#         self.make = make
#         self.model = model
#         self.year = year

#     def __str__(self):
#         return f"{self.year} {self.model} {self.make}"


# carex = Car("Oldsmobile", "Alero", 2001)

# print(carex.__str__())


def horizontal_bar_chart(sentence):
    new_list = []
    new_dict = {}
    for char in sentence:
        if char not in new_dict:
            new_dict[char] = 1
        else:
            new_dict[char] += 1

    for char, count in new_dict.items():
        if char != " ":
            new_list.append(char * count)

    new_list.sort()
    return new_list


print(horizontal_bar_chart("abba has a banana"))
