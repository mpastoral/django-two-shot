from django.urls import path
from receipts.views import (
    receipt_view,
    create_receipt,
    account_view,
    category_view,
    create_category,
    create_account,
)

# feature 6 main page note
# the name of receipt_view used is "home" in the
# redirect to main page function
urlpatterns = [
    path("", receipt_view, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", account_view, name="account_list"),
    path("categories/", category_view, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
